package main

import (
	"context"
	"database/sql"
	"flag"
	"log"
	"net/http"

	"github.com/go-redis/redis"
	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/robfig/cron/v3"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/api/send_message"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/apilayerintegration"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/bridge_services"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/clients/tg"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/config"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/controllers"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/currency_rate_service"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/messages"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func runCronUpdateCurrency(crs *currency_rate_service.ExchangeRateUpdateSvc) {
	go crs.UpdateCurrencyRates()
	cronHandler := cron.New()

	//https://pkg.go.dev/github.com/robfig/cron/v3 5:00 every day server time
	_, err := cronHandler.AddFunc("0 5 * * *", func() {
		crs.UpdateCurrencyRates()
	})
	if err != nil {
		logger.Logger.Error("runCronUpdateCurrency failed:", zap.Error(err))
	}
	cronHandler.Start()
	defer cronHandler.Stop()
	select {}
}

var (
	addr = flag.String("addr", "localhost:50001", "the address to connect to")
)

func main() {
	// DB
	db, err := sql.Open("postgres", "host=localhost port=5432 user=admin password=pass sslmode=disable dbname=ozon_hw")
	if err != nil {
		log.Fatal(err)
	}

	// redis
	redisClient := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
		DB:   0,
	})
	redisClient.Del()
	// Cache
	cache := controllers.NewCache(redisClient)

	// ExpensesController
	expenseDB := controllers.NewExpenseDB(db)
	expensesController := controllers.NewExpensesController(expenseDB, cache)

	// User controller
	userController := controllers.NewUserDB(db)

	tgBotConfig, err := config.New()
	if err != nil {
		logger.Logger.Error("config init failed:", zap.Error(err))
	}
	tgClient, err := tg.New(tgBotConfig)
	if err != nil {
		logger.Logger.Error("tg client init failed:", zap.Error(err))
	}

	rateAPIGateway := apilayerintegration.New(expenseDB)
	exchangeRateUpdateSvc := currency_rate_service.New(rateAPIGateway, expenseDB)
	go runCronUpdateCurrency(exchangeRateUpdateSvc)

	if err != nil {
		logger.Logger.Error("runCronUpdateCurrency failed:", zap.Error(err))
	}
	msgModel := messages.New(tgClient, expensesController, userController)

	// Set up a connection to the server.
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	grpcClient := send_message.NewSendBotCommandClient(conn)

	_, err = bridge_services.NewKafkaGRPCIntegrator(context.Background(), msgModel, &grpcClient)
	if err != nil {
		logger.Logger.Error("runCronUpdateCurrency failed:", zap.Error(err))
	}

	http.Handle("/metrics", promhttp.Handler())
	logger.Logger.Info("starting http server", zap.Int("port", 8080))
	err = http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		logger.Logger.Error("error starting http server", zap.Error(err))
	}

}
