package main

import (
	"log"
	"net"
	"net/http"

	_ "github.com/lib/pq"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/api/send_message"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/clients/tg"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/config"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/controllers"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/grpc_server"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/kafka/utils"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

func main() {
	tgBotConfig, err := config.New()
	if err != nil {
		logger.Logger.Error("config init failed:", zap.Error(err))
	}
	tgClient, err := tg.New(tgBotConfig)
	if err != nil {
		logger.Logger.Error("tg client init failed:", zap.Error(err))
	}

	producer, err := utils.NewProducer(utils.BrokersList)
	if err != nil {
		logger.Logger.Error("kafka producer init failed:", zap.Error(err))
	}

	kafka := controllers.NewKafkaController(producer, utils.KafkaTopic)

	go tgClient.ListenUpdates(kafka)

	lis, err := net.Listen("tcp", ":50001")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	send_message.RegisterSendBotCommandServer(s, &grpc_server.Server{TgClient: tgClient})
	log.Printf("server listening at %v", lis.Addr())

	go func() {
		err := s.Serve(lis)
		if err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	http.Handle("/metrics", promhttp.Handler())
	logger.Logger.Info("starting http server", zap.Int("port", 8080))
	err = http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		logger.Logger.Error("error starting http server", zap.Error(err))
	}

}
