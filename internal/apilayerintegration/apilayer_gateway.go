package apilayerintegration

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/common"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

type DBStorage interface {
	IsPresentsRatesForDate(ctx context.Context, date string) (bool, error)
}

type Gateway struct {
	storage DBStorage
}

func New(storage DBStorage) *Gateway {
	return &Gateway{storage: storage}
}

// https://www.cbr-xml-daily.ru/#json

type Config struct {
	Token   string `yaml:"token"`
	Url     string `yaml:"url"`
	Symbols string `yaml:"symbols"`
	Base    string `yaml:"base"`
}

type Response struct {
	Base       string `json:"base"`
	Date       string `json:"date"`
	Historical bool   `json:"historical"`
	Rates      struct {
		CNY float64 `json:"CNY"`
		EUR float64 `json:"EUR"`
		USD float64 `json:"USD"`
	} `json:"rates"`
	Success   bool `json:"success"`
	Timestamp int  `json:"timestamp"`
}

const configFile = "data/currency_api.yaml"

func (gate *Gateway) parseConfig() Config {
	logger.Logger.Info("UpdateCurrencyRates ... parseConfig ")

	rawYAML, err := os.ReadFile(configFile)
	if err != nil {
		logger.Logger.Error("integration error", zap.Error(err))
	}
	c := Config{}
	err = yaml.Unmarshal(rawYAML, &c)
	if err != nil {
		logger.Logger.Error("parsing yaml", zap.Error(err))
	}
	return c
}

type API struct {
	Client *http.Client
}

func (gate *Gateway) getCurrencyRates(c Config, api *API) (Response, error) {

	currentTime := time.Now()
	yesterday := currentTime.AddDate(0, 0, -1)
	dateStr := yesterday.Format(common.CommonDateFormat)
	isPresent, err := gate.storage.IsPresentsRatesForDate(context.Background(), dateStr)
	if err == nil && isPresent {
		logger.Logger.Info("already downloaded", logger.CurrencyIntegration)
		return Response{}, fmt.Errorf("already downloaded")
	}
	if err != nil {
		logger.Logger.Error("", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}

	params := url.Values{
		"symbols": {c.Symbols},
		"base":    {c.Base},
	}

	reqUrl := c.Url + dateStr + "?" + params.Encode()

	client := api.Client
	req, err := http.NewRequest("GET", reqUrl, nil)
	req.Header.Set("apikey", c.Token)

	if err != nil {
		logger.Logger.Warn("REST integration error", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}
	res, err := client.Do(req)
	if err != nil {

		logger.Logger.Warn("integration error", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		logger.Logger.Warn("integration error", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}
	var responseBody Response
	err = json.Unmarshal(body, &responseBody)
	if err != nil {
		logger.Logger.Warn("integration error", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}
	return responseBody, nil
}

func (gate *Gateway) GetCurrencyRates() (Response, error) {
	c := gate.parseConfig()
	rates, err := gate.getCurrencyRates(c, &API{Client: &http.Client{}})

	if err != nil {
		logger.Logger.Warn("REST integration error or allready dowloaded", logger.CurrencyIntegration, zap.Error(err))
		return Response{}, err
	}
	return rates, nil
}
