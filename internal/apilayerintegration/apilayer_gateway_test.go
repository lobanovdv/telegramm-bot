package apilayerintegration

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/common"
	mock "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/mocks/apilayerintegration"
)

func TestDoStuffWithTestServer(t *testing.T) {
	// Start a local HTTP server
	ctrl := gomock.NewController(t)
	db := mock.NewMockDBStorage(ctrl)
	url := ""
	today := time.Now().AddDate(0, 0, -1).Format(common.CommonDateFormat)
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.RequestURI, "/"+today+"?base=RUB&symbols=asdasdasd")
		assert.Equal(t, req.Header.Get("Apikey"), "12345")
		_, _ = rw.Write([]byte(`{
  			"base": "RUB",
            "date": "2022-10-08",
  			"historical": true,
  			"rates": {
    			"CNY": 0.11413,
    			"EUR": 0.016466,
    			"USD": 0.016038
  			},
    		"success": true,
  			"timestamp": 1665273599
		}`))
	}))
	// Close the server when test finishes
	defer server.Close()

	// Use Client & URL from our local test server
	api := API{server.Client()}
	url = server.URL + "/"

	rateAPIGateway := New(db)
	db.EXPECT().IsPresentsRatesForDate(gomock.Any(), gomock.Any()).Return(false, nil)
	resp, err := rateAPIGateway.getCurrencyRates(Config{Token: "12345",
		Url:     url,
		Symbols: "asdasdasd",
		Base:    "RUB"}, &api)
	assert.NoError(t, err)
	assert.Equal(t, resp.Rates.CNY, float64(0.11413))
	assert.NoError(t, err)
	assert.Equal(t, resp.Rates.EUR, float64(0.016466))
	assert.NoError(t, err)
	assert.Equal(t, resp.Rates.USD, float64(0.016038))
}
