package bridge_services

import (
	"context"
	"fmt"
	"strconv"

	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/api/send_message"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/messages"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/texts"
	"go.uber.org/zap"
)

type KafkaGRPCIntegrator struct {
	messageModel *messages.Model
	grpcClient   send_message.SendBotCommandClient
}

var (
	KafkaTopic         = "commands"
	KafkaConsumerGroup = "example-consumer-group"
	BrokersList        = []string{"localhost:9092"}
)

func NewKafkaGRPCIntegrator(ctx context.Context,
	messageModel *messages.Model,
	grpcClient *send_message.SendBotCommandClient) (*KafkaGRPCIntegrator, error) {
	integrator := KafkaGRPCIntegrator{messageModel: messageModel, grpcClient: *grpcClient}

	config := sarama.NewConfig()
	config.Version = sarama.V2_5_0_0
	config.Consumer.Offsets.Initial = sarama.OffsetOldest

	// Create consumer group
	consumerGroup, err := sarama.NewConsumerGroup(BrokersList, KafkaConsumerGroup, config)
	if err != nil {
		return &integrator, fmt.Errorf("starting consumer group: %w", err)
	}

	err = consumerGroup.Consume(ctx, []string{KafkaTopic}, &integrator)
	if err != nil {
		return &integrator, fmt.Errorf("consuming via handler: %w", err)
	}
	return &integrator, nil
}

// Setup is run at the beginning of a new session, before ConsumeClaim.
func (consumer *KafkaGRPCIntegrator) Setup(sarama.ConsumerGroupSession) error {
	fmt.Println("consumer - setup")
	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited.
func (consumer *KafkaGRPCIntegrator) Cleanup(sarama.ConsumerGroupSession) error {
	fmt.Println("consumer - cleanup")
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *KafkaGRPCIntegrator) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {
		userId, err := strconv.ParseInt(string(message.Key), 10, 64)
		ctx := context.Background()
		if err != nil {
			logger.Logger.Error("error parce userid", zap.Error(err))
		}

		ctx, keyboard, answer, err := consumer.messageModel.IncomingMessageProcessing(
			ctx,
			messages.Message{
				Text:   string(message.Value),
				UserID: userId,
			})

		logger.Logger.Info(
			"s.incomingMessageProcessing result",
			zap.Any("keyboard", keyboard),
			zap.String("message", answer),
			zap.String("trace_id", ""),
			zap.Error(err),
		)
		if err != nil {
			logger.Logger.Error("IncomingMessageProcessing err", zap.Error(err))
			consumer.makeGRPCCall(ctx, userId, 0, texts.SomethingWentWrongText)
			continue
		}
		consumer.makeGRPCCall(ctx, userId, keyboard, answer)
		session.MarkMessage(message, "")
	}

	return nil
}

func (consumer *KafkaGRPCIntegrator) makeGRPCCall(ctx context.Context, userId int64, keyboard messages.KeyboardType, msg string) {
	logger.Logger.Info(
		"s.makeGRPCCall result",
		zap.Any("keyboard", keyboard),
		zap.String("message", msg),
		zap.String("trace_id", ""),
	)

	_, err := consumer.grpcClient.SendBotCommand(ctx, &send_message.SendBotCommandRequest{
		UserId:       userId,
		KeyboardType: int32(keyboard),
		Message:      msg})

	if err != nil {
		logger.Logger.Error("could not greet: %v", zap.Error(err))
	}
}
