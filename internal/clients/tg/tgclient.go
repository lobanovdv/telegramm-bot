package tg

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/controllers"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/texts"
	"go.uber.org/zap"
)

type TokenGetter interface {
	Token() string
}

type Client struct {
	client *tgbotapi.BotAPI
}

var botCommands = []tgbotapi.BotCommand{{
	Command:     "add",
	Description: "add expense to inputed category",
},
	{
		Command:     "add_with_date",
		Description: "add expense to inputed category with needed date",
	},
	{
		Command:     "set_limit",
		Description: "set month limit for category",
	},
	{
		Command:     "report",
		Description: "return categories report",
	},
	{
		Command:     "change_currency",
		Description: "opens change currency dialog",
	},
}

var currencyKeyboard = tgbotapi.NewOneTimeReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("USD"),
		tgbotapi.NewKeyboardButton("EUR"),
	),
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("CNY"),
		tgbotapi.NewKeyboardButton("RUB"),
	),
)

var reportPeriodKeyboard = tgbotapi.NewOneTimeReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("week"),
		tgbotapi.NewKeyboardButton("month"),
		tgbotapi.NewKeyboardButton("year"),
	),
)

func New(tokenGetter TokenGetter) (*Client, error) {
	client, err := tgbotapi.NewBotAPI(tokenGetter.Token())
	if err != nil {
		return nil, errors.Wrap(err, "NewBotAPI")
	}
	// TOdo
	_, _ = client.Request(tgbotapi.NewSetMyCommands(botCommands...))
	return &Client{
		client: client,
	}, nil
}

func (c *Client) SendMessage(ctx context.Context, text string, userID int64) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "SendMessage")
	defer span.Finish()
	msg := tgbotapi.NewMessage(userID, text)
	msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	_, err := c.client.Send(msg)
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) ShowCurrencyKeyboard(ctx context.Context, userID int64) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "ShowCurrencyKeyboard")
	defer span.Finish()
	msg := tgbotapi.NewMessage(userID, "choose currency")
	msg.ReplyMarkup = currencyKeyboard
	_, err := c.client.Send(msg)
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) ShowReportPeriodKeyboard(ctx context.Context, userID int64) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "ShowReportPeriodKeyboard")
	defer span.Finish()
	msg := tgbotapi.NewMessage(userID, "choose period")
	msg.ReplyMarkup = reportPeriodKeyboard
	_, err := c.client.Send(msg)
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) ShowCategoriesKeyboard(ctx context.Context, userID int64, categories []string) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "ShowCategoriesKeyboard")
	defer span.Finish()
	msg := tgbotapi.NewMessage(userID, texts.ChooseCategoryText)
	if len(categories) != 0 {
		rows := make([][]tgbotapi.KeyboardButton, 0)

		for i := 0; i < len(categories); i++ {
			if i%3 == 0 {
				rows = append(rows, make([]tgbotapi.KeyboardButton, 3))
			}
			rows[i/3][i%3] = tgbotapi.NewKeyboardButton(categories[i])
		}
		var categoryKeyboard = tgbotapi.NewOneTimeReplyKeyboard(rows...)
		msg.ReplyMarkup = categoryKeyboard
	}
	_, err := c.client.Send(msg)
	if err != nil {
		return errors.Wrap(err, "client.Send")
	}
	return nil
}

func (c *Client) ListenUpdates(kafka *controllers.KafkaController) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := c.client.GetUpdatesChan(u)

	logger.Logger.Info("listening for messages", logger.TgIntegration)

	for update := range updates {
		if update.Message != nil { // If we got a message
			logger.Logger.Info("message",
				zap.String("userName", update.Message.From.UserName),
				zap.String("text", update.Message.Text),
				logger.TgIntegration,
			)
			ctx := context.Background()
			//startTime := time.Now()

			err := kafka.SendMessage(ctx, update.Message.From.ID, update.Message.Text)
			//duration := time.Since(startTime)
			// metrics.SummaryResponseTime.Observe(duration.Seconds())
			//metrics.HistogramResponseTime.
			//	WithLabelValues(ctx.Value(metrics.MetricsCommandType).(string)).
			//	Observe(duration.Seconds())

			if err != nil {
				logger.Logger.Error("error processing message:", logger.TgIntegration, zap.Error(err))
			}
		}
	}
}
