package controllers

import (
	"context"
	"time"

	"github.com/go-redis/redis"
	"github.com/opentracing/opentracing-go"
)

type RedisStorage interface {
	Set(key string, value interface{}, expiration time.Duration) *redis.StatusCmd
	Get(s string) *redis.StringCmd
	Del(keys ...string) *redis.IntCmd
}

type Cache struct {
	client RedisStorage
}

func NewCache(storage RedisStorage) *Cache {
	return &Cache{client: storage}
}

func (self *Cache) SetValue(ctx context.Context, key string, value string) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "CacheSet")
	defer span.Finish()
	err := self.client.Set(key, value, time.Hour).Err()
	return err
}

func (self *Cache) GetValue(ctx context.Context, key string) (string, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "CacheGet")
	defer span.Finish()
	val, err := self.client.Get(key).Result()
	return val, err
}

func (self *Cache) Del(ctx context.Context, keys ...string) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "CacheDel")
	defer span.Finish()
	_, err := self.client.Del(keys...).Result()
	return err
}
