package controllers

import (
	"context"
	"testing"

	"github.com/go-redis/redis"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	mock_controllers "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/mocks/controllers"
)

func TestSet(t *testing.T) {
	ctrl := gomock.NewController(t)
	redis_mock := mock_controllers.NewMockRedisStorage(ctrl)
	cacheController := NewCache(redis_mock)

	redis_mock.EXPECT().Set("key", "value", gomock.Any()).Return(&redis.StatusCmd{})
	err := cacheController.SetValue(context.Background(), "key", "value")
	assert.NoError(t, err)
}

func TestGet(t *testing.T) {
	ctrl := gomock.NewController(t)
	redis_mock := mock_controllers.NewMockRedisStorage(ctrl)
	cacheController := NewCache(redis_mock)

	redis_mock.EXPECT().Get("key").Return(&redis.StringCmd{})
	val, err := cacheController.GetValue(context.Background(), "key")
	assert.Equal(t, "", val)
	assert.NoError(t, err)
}

func TestDel(t *testing.T) {
	ctrl := gomock.NewController(t)
	redis_mock := mock_controllers.NewMockRedisStorage(ctrl)
	cacheController := NewCache(redis_mock)

	redis_mock.EXPECT().Del("key1", "key2", "key3").Return(&redis.IntCmd{})
	err := cacheController.Del(context.Background(), "key1", "key2", "key3")
	assert.NoError(t, err)
}
