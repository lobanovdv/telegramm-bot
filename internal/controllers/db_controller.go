package controllers

import (
	"context"
	"database/sql"
	"strconv"
	time "time"

	log "github.com/inconshreveable/log15"
	"github.com/lib/pq"
	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/common"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/db_models"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	expenses "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/expenses"
	"go.uber.org/zap"
)

type SqlDB interface {
	Begin() (*sql.Tx, error)
}

type ExpenseDB struct {
	db SqlDB
}

func NewExpenseDB(db SqlDB) *ExpenseDB {
	return &ExpenseDB{db: db}
}

func closeSQLRowsForDefer(rows *sql.Rows) {
	err := rows.Close()
	if err != nil {
		log.Error("rows.Close()", err)
	}
}

func (self *ExpenseDB) AddExpense(ctx context.Context, expense expenses.Expense) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "AddExpense")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.AddExpense",
		zap.Any("input", expense),
	)
	tx, err := self.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	category, _ := self.getOrCreateCategory(ctx, tx, expense.Category, expense.UserID)
	const insertQuery = `
		insert into public.expenses(
			category_id,
			amount_rub,
		    date
		) values (
			$1, $2, $3
		);
	`
	_, err = tx.Exec(insertQuery,
		&category.Id,
		&expense.Amount,
		&expense.Date,
	)
	if err = tx.Commit(); err != nil {
		return err
	}
	return err
}

func (self *ExpenseDB) GetExpenses(ctx context.Context, userID int64, dateGt time.Time, dateLt time.Time) ([]expenses.Expense, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "GetExpenses")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.GetExpenses",
		zap.Int64("userID", userID),
		zap.Time("dateGt", dateGt),
		zap.Time("dateLt", dateLt),
	)
	result := make([]expenses.Expense, 0)
	tx, err := self.db.Begin()
	if err != nil {
		return result, err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	const selectCategoriesQuery = `
		select id,      
			   user_id,   
               name,      
               limit_rub 
		from public.categories
		where user_id = $1; 
	`

	categoriesIdNameMap := make(map[int64]string)
	categoryIds := make([]int64, 0)
	rows, err := tx.Query(selectCategoriesQuery, userID)
	if err != nil {
		return result, err
	}
	defer closeSQLRowsForDefer(rows)
	var (
		id       int64
		userId   int64
		name     string
		limitRub sql.NullInt64
	)
	for rows.Next() {
		err = rows.Scan(&id, &userId, &name, &limitRub)
		if err != nil {
			return result, err
		}
		categoriesIdNameMap[id] = name
		categoryIds = append(categoryIds, id)
	}

	const selectExpensesQuery = `
		select id, category_id, amount_rub, date from public.expenses
		where category_id = ANY($1) and date>$2 and date<=$3;
	`
	rows, err = tx.Query(selectExpensesQuery, pq.Array(categoryIds), dateGt.Format(common.CommonDateFormat), dateLt.Format(common.CommonDateFormat))
	if err != nil {
		return result, err
	}
	defer closeSQLRowsForDefer(rows)

	var (
		categoryId int64
		amountRub  int64
		date       time.Time
	)
	for rows.Next() {
		err = rows.Scan(&id, &categoryId, &amountRub, &date)
		if err != nil {
			return result, err
		}
		result = append(result, expenses.Expense{
			ID:       id,
			Category: categoriesIdNameMap[categoryId],
			UserID:   userID,
			Amount:   amountRub,
			Date:     date,
		})
	}

	// todo тут из базы можно групбайнуть и ваще по красоте вытащить
	return result, nil
}

func (self *ExpenseDB) GetCategoriesList(ctx context.Context, userID int64) ([]string, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "GetCategoriesList")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.GetCategoriesList",
		zap.Int64("userID", userID),
	)
	result := make([]string, 0)
	tx, err := self.db.Begin()
	if err != nil {
		return result, err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	const selectCategoriesQuery = `
		select id,      
			   user_id,   
               name,      
               limit_rub 
		from public.categories
		where user_id = $1; 
	`

	rows, err := tx.Query(selectCategoriesQuery, userID)
	defer closeSQLRowsForDefer(rows)
	if err != nil {
		return result, err
	}
	var (
		id       int64
		userId   int64
		name     string
		limitRub sql.NullInt64
	)
	for rows.Next() {
		err = rows.Scan(&id, &userId, &name, &limitRub)
		if err != nil {
			return result, err
		}
		result = append(result, name)
	}
	return result, nil
}

//type currencyRate struct {
//	Date string  `json:"date"`
//	CNY  float64 `json:"CNY"`
//	EUR  float64 `json:"EUR"`
//	USD  float64 `json:"USD"`
//}

func (self *ExpenseDB) addRate(ctx context.Context, tx *sql.Tx, code string, rate float64, date string) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "addRate")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.addRate",
		zap.String("code", code),
		zap.Float64("rate", rate),
		zap.String("date", date),
	)
	const insertQuery = `
		insert into public.rates(
    		code,
    		rate,
    		date,
    		created_at
		) values (
			$1, $2, $3, $4
		);
	`
	_, err := tx.Exec(insertQuery,
		code,
		rate,
		date,
		time.Now(),
	)
	return err
}

func (self *ExpenseDB) AddCurrencyRate(ctx context.Context, CNY, EUR, USD float64, date string) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "AddCurrencyRate")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.AddCurrencyRate",
		zap.Float64("CNY", CNY),
		zap.Float64("EUR", EUR),
		zap.Float64("USD", USD),
		zap.String("date", date),
	)
	tx, err := self.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	err = self.addRate(ctx, tx, "CNY", CNY, date)
	if err != nil {
		return err
	}
	err = self.addRate(ctx, tx, "EUR", EUR, date)
	if err != nil {
		return err
	}
	err = self.addRate(ctx, tx, "USD", USD, date)
	if err != nil {
		return err
	}
	return nil
}

func (self *ExpenseDB) GetLastCurrencyRate(ctx context.Context, currency string) (float64, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "GetLastCurrencyRate")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.GetLastCurrencyRate",
		zap.String("currency", currency),
	)
	tx, err := self.db.Begin()
	if err != nil {
		return 0, err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	if currency == "RUB" || currency == "" {
		return float64(1), nil
	}
	const selectQuery = `
		select rate from public.rates
		where code = $1
		order by date desc 
		limit 1;
    `
	var result float64
	err = tx.QueryRow(selectQuery, currency).Scan(&result)
	return result, err
}

func (self *ExpenseDB) IsPresentsRatesForDate(ctx context.Context, date string) (bool, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "IsPresentsRatesForDate")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.IsPresentsRatesForDate",
		zap.String("date", date),
	)
	tx, err := self.db.Begin()
	if err != nil {
		return false, err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	const selectQuery = `
		select id from public.rates
		where date = $1
    `
	var id int64
	err = tx.QueryRow(selectQuery, date).Scan(&id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, nil
}

func (self *ExpenseDB) SetLimit(ctx context.Context, amount int64, categoryName string, userID int64) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "SetLimit")
	defer span.Finish()
	logger.Logger.Debug("ExpenseDB.SetLimit")
	tx, err := self.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	category, _ := self.getOrCreateCategory(ctx, tx, categoryName, userID)
	const updateQuery = `
		update public.categories set limit_rub=$1
		where id=$2;
	`
	_, err = tx.Exec(updateQuery,
		amount,
		category.Id,
	)
	if err != nil {
		return err
	}
	return nil
}

func (self *ExpenseDB) getCategoryLimit(ctx context.Context, tx *sql.Tx, categoryId int64) (sql.NullInt64, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "getCategoryLimit")
	defer span.Finish()
	var result sql.NullInt64
	const selectLimit = `
		select limit_rub from public.categories
		where id = $1
    `
	err := tx.QueryRow(selectLimit, categoryId).Scan(&result)
	return result, err
}

func (self *ExpenseDB) selectSumExpensesQuery(ctx context.Context, tx *sql.Tx, categoryId int64, firstDay, lastDay time.Time) (int64, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "selectSumExpensesQuery")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.selectSumExpensesQuery",
		zap.Int64("categoryId", categoryId),
		zap.Time("firstDay", firstDay),
		zap.Time("lastDay", lastDay),
	)
	var result int64
	const selectSumExpensesQuery = `
		select sum(amount_rub) from public.expenses
		where category_id = $1 and date>=$2 and date<=$3;
	`
	err := tx.QueryRow(selectSumExpensesQuery, categoryId, firstDay, lastDay).Scan(&result)
	logger.Logger.Debug(
		"ExpenseDB.selectSumExpensesQuery",
		zap.Int64("result", result),
	)
	return result, err
}

func (self *ExpenseDB) GetCategoryLimit(ctx context.Context, categoryName string, userID int64) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetCategoryLimit")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.GetCategoryLimit",
		zap.String("categoryName", categoryName),
		zap.Int64("userID", userID),
	)
	tx, err := self.db.Begin()
	if err != nil {
		return "", err
	}
	defer func() {
		if err != nil {
			err = tx.Rollback()
			log.Error("Rollback", err)
			return
		}
		err = tx.Commit()
	}()
	category, _ := self.getOrCreateCategory(ctx, tx, categoryName, userID)
	now := time.Now()
	firstDay := time.Date(now.Year(), now.Month(), 1, 0, 0, 0, 0, time.Local)
	lastDay := firstDay.AddDate(0, 1, 0).Add(time.Nanosecond * -1)
	var limit sql.NullInt64
	total, err := self.selectSumExpensesQuery(ctx, tx, category.Id, firstDay, lastDay)
	if err != nil {
		return "", err
	}
	limit, err = self.getCategoryLimit(ctx, tx, category.Id)
	if err != nil {
		return "", err
	}
	if !limit.Valid {
		return "", nil
	}
	logger.Logger.Debug(
		"ExpenseDB.GetCategoryLimit",
		zap.Int64("limit", limit.Int64-total),
	)
	return strconv.FormatInt(limit.Int64-total, 10) + " remaining", nil
}

func (self *ExpenseDB) selectCategory(ctx context.Context, tx *sql.Tx, categoryName string, userID int64) (*db_models.Category, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "selectCategory")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.selectCategory",
		zap.String("categoryName", categoryName),
		zap.Int64("userID", userID),
	)
	const selectQuery = `
		select id,      
			   user_id,   
               name,      
               limit_rub 
		from public.categories
		where name = $1 AND user_id = $2;
	`
	var category db_models.Category
	err := tx.QueryRow(selectQuery, categoryName, userID).Scan(&category.Id, &category.UserId, &category.Name, &category.LimitRub)
	if err == sql.ErrNoRows {
		logger.Logger.Debug(
			"ExpenseDB.selectCategory",
			zap.Bool("ErrNoRows", true),
		)
		return nil, nil
	}
	return &category, err
}

func (self *ExpenseDB) insertCategory(ctx context.Context, tx *sql.Tx, userID int64, name string, limitRub int64) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "insertCategory")
	defer span.Finish()
	const insertQuery = `
		insert into public.categories(
			user_id, 
			name,
			limit_rub
		) values (
			$1, $2, $3
		);
	`
	_, err := tx.Exec(insertQuery,
		userID,
		name,
		limitRub,
	)
	return err
}

func (self *ExpenseDB) getOrCreateCategory(ctx context.Context, tx *sql.Tx, categoryName string, userID int64) (*db_models.Category, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "getOrCreateCategory")
	defer span.Finish()
	logger.Logger.Debug(
		"ExpenseDB.getOrCreateCategory",
		zap.String("categoryName", categoryName),
		zap.Int64("userID", userID),
	)
	category, err := self.selectCategory(ctx, tx, categoryName, userID)
	if err != nil {
		return nil, err
	}
	if category == nil {
		logger.Logger.Debug(
			"ExpenseDB.getOrCreateCategory insert new one",
		)
		err := self.insertCategory(ctx, tx, userID, categoryName, 0)
		if err != nil {
			return nil, err
		}
		category, err = self.selectCategory(ctx, tx, categoryName, userID)
		if err != nil {
			return nil, err
		}
	}
	return category, err
}
