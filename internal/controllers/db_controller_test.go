package controllers

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/pressly/goose/v3"
	_ "github.com/pressly/goose/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"go.uber.org/zap"
)

const (
	host     = "postgres"
	port     = "5432"
	user     = "admin"
	password = "pass"
	dbname   = "test_db"
)

const (
	migrationsPath = "../../migrations"
	fixturesPath   = "../../fixtures"
)

func TestDB(t *testing.T) {
	log.SetFlags(0)

	// PostgreSQL

	dbPostgres, err := sql.Open("postgres", fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		user, password, dbname, host, port))
	if err != nil {
		panic(err)
	}
	defer func() {
		err = dbPostgres.Close()
		if err != nil {
			logger.Logger.Error("dbPostgres.Close() error", zap.Error(err))
		}
	}()

	goose.SetBaseFS(nil)

	if err := goose.SetDialect("postgres"); err != nil {
		panic(err)
	}
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)

	if err := goose.Up(dbPostgres, migrationsPath); err != nil {
		panic(err)
	}
	defer func() {
		print("goose.Down")
		if err := goose.DownTo(dbPostgres, migrationsPath, 0); err != nil {
			panic(err)
		}
	}()

	files, err := os.ReadDir(fixturesPath)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if !file.IsDir() {
			b, err := os.ReadFile(fixturesPath + "/" + file.Name()) // just pass the file name
			if err != nil {
				fmt.Print(err)
			}
			str := string(b)
			if _, err = dbPostgres.Exec(str); err != nil {
				panic(err)
			}
		}
	}

	expenseDB := NewExpenseDB(dbPostgres)
	categories, err := expenseDB.GetCategoriesList(context.Background(), int64(45770962))
	assert.NoError(t, err)
	assert.Equal(t, []string{"asdf", "qwer", "rty", "cds", "food", "asd", "avf", "t", "😎"}, categories)
}
