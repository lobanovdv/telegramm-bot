package controllers

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/expenses"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/texts"
	"go.uber.org/zap"
)

const (
	reportParamWeek  = "week"
	reportParamMonth = "month"
	reportParamYear  = "year"
)

type DBStorage interface {
	AddExpense(ctx context.Context, expense expenses.Expense) error
	GetExpenses(ctx context.Context, userID int64, dateGt time.Time, dateLt time.Time) ([]expenses.Expense, error)
	GetCategoriesList(ctx context.Context, userID int64) ([]string, error)
	AddCurrencyRate(ctx context.Context, CNY, EUR, USD float64, date string) error
	GetLastCurrencyRate(ctx context.Context, currency string) (float64, error)
	SetLimit(ctx context.Context, amount int64, category string, userID int64) error
	GetCategoryLimit(ctx context.Context, categoryName string, userID int64) (string, error)
}

type FastCache interface {
	SetValue(ctx context.Context, key string, value string) error
	GetValue(ctx context.Context, key string) (string, error)
	Del(ctx context.Context, keys ...string) error
}

type ExpensesController struct {
	storage DBStorage
	cache   FastCache
}

func NewExpensesController(storage DBStorage, cache FastCache) *ExpensesController {
	return &ExpensesController{storage: storage, cache: cache}
}

func (self *ExpensesController) dropUserCache(ctx context.Context, userID int64) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "dropUserCache")
	defer span.Finish()
	err := self.cache.Del(ctx,
		strconv.FormatInt(userID, 10)+"_"+reportParamWeek,
		strconv.FormatInt(userID, 10)+"_"+reportParamMonth,
		strconv.FormatInt(userID, 10)+"_"+reportParamYear,
	)
	return err
}

func (self *ExpensesController) addExpense(ctx context.Context, date time.Time, amount int64, category string, userID int64) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "addExpense")
	defer span.Finish()
	err := self.dropUserCache(ctx, userID)
	if err != nil {
		return "", err
	}
	expense := expenses.New(date, category, amount, userID)
	err = self.storage.AddExpense(ctx, *expense)
	if err != nil {
		return "", err
	}
	return self.storage.GetCategoryLimit(ctx, category, userID)
}

func (self *ExpensesController) AddExpense(ctx context.Context, amount int64, category string, userID int64, rate float64) (string, error) {
	amountRub := int64(float64(amount) / rate)
	return self.addExpense(ctx, time.Now(), amountRub, category, userID)
}

func (self *ExpensesController) AddExpenseWithDate(ctx context.Context, data string, userID int64, rate float64) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "AddExpenseWithDate")
	defer span.Finish()
	logger.Logger.Info("AddExpenseWithDate ", zap.String("data", data))
	data = strings.TrimSpace(data)
	splitedData := strings.Split(data, " ")
	if len(splitedData) != 3 {
		return fmt.Errorf("incorect format")
	}
	date, err := time.Parse("02.01.2006", splitedData[0])
	if err != nil {
		return fmt.Errorf("incorect format")
	}
	amount, err := strconv.ParseInt(splitedData[1], 10, 0)
	amountRub := int64(float64(amount) / rate)
	category := splitedData[2]
	if err != nil {
		return err
	}
	_, err = self.addExpense(ctx, date, amountRub, category, userID)
	return err
}

func (self *ExpensesController) GetReport(ctx context.Context, data string, userID int64, currencyRate float64) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetReport")
	defer span.Finish()
	logger.Logger.Info("GetReport: ", zap.String("data", data))

	data = strings.TrimSpace(data)
	startDate := time.Now()
	endDate := time.Now()
	switch data {
	case reportParamWeek:
		startDate = startDate.AddDate(0, 0, -7)
	case reportParamMonth:
		startDate = startDate.AddDate(0, -1, 0)
	case reportParamYear:
		startDate = startDate.AddDate(-1, 0, 0)
	default:
		return "", fmt.Errorf("incorect format")
	}

	cachedValue, err := self.cache.GetValue(ctx, strconv.FormatInt(userID, 10)+"_"+data)
	if err == nil {
		logger.Logger.Info("Value from cache")
		return cachedValue, nil
	}

	categoryAmountMap := make(map[string]int64, 0)
	filteredExpenses, err := self.storage.GetExpenses(ctx, userID, startDate, endDate)
	if err != nil {
		return "", err
	}
	for _, val := range filteredExpenses {
		categoryAmountMap[val.Category] += val.Amount
	}
	result := ""
	for k, v := range categoryAmountMap {
		row, err := texts.CreateReportRowMessage(k, strconv.FormatFloat(float64(v)*currencyRate, 'f', 2, 64))
		if err != nil {
			return "", err
		}
		result += row
	}
	err = self.cache.SetValue(ctx, strconv.FormatInt(userID, 10)+"_"+data, result)
	if err != nil {
		logger.Logger.Error("cache.SetValue error")
	}
	return result, nil
}

func (self *ExpensesController) GetCategoriesList(ctx context.Context, userID int64) ([]string, error) {
	return self.storage.GetCategoriesList(ctx, userID)
}

func (self *ExpensesController) AddCurrencyRate(ctx context.Context, CNY, EUR, USD float64, date string) error {
	return self.storage.AddCurrencyRate(ctx, CNY, EUR, USD, date)
}
func (self *ExpensesController) GetLastCurrencyRate(ctx context.Context, currency string) (float64, error) {
	return self.storage.GetLastCurrencyRate(ctx, currency)
}

func (self *ExpensesController) SetLimit(ctx context.Context, amount int64, category string, userID int64) error {
	return self.storage.SetLimit(ctx, amount, category, userID)
}
