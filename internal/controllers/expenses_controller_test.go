package controllers

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	mock_controllers "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/mocks/controllers"
)

func TestAddExpenseSunnyDay(t *testing.T) {
	ctrl := gomock.NewController(t)
	db := mock_controllers.NewMockDBStorage(ctrl)
	cache := mock_controllers.NewMockFastCache(ctrl)
	expensesController := NewExpensesController(db, cache)

	db.EXPECT().AddExpense(gomock.Any(), gomock.Any())
	db.EXPECT().GetCategoryLimit(gomock.Any(), gomock.Any(), int64(123))
	cache.EXPECT().Del(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any())
	_, err := expensesController.AddExpense(context.Background(), int64(100), "groseries", int64(123), float64(1))
	assert.NoError(t, err)
}
