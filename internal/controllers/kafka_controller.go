package controllers

import (
	"context"
	"log"
	"strconv"

	"github.com/Shopify/sarama"
)

type KafkaController struct {
	producer sarama.AsyncProducer
	topic    string
}

func NewKafkaController(producer sarama.AsyncProducer, topic string) *KafkaController {
	return &KafkaController{producer: producer, topic: topic}
}

func (self *KafkaController) SendMessage(ctx context.Context, userId int64, msg string) error {
	// Inject info into message
	producerMsg := sarama.ProducerMessage{
		Topic:   self.topic,
		Key:     sarama.StringEncoder(strconv.FormatInt(userId, 10)),
		Value:   sarama.StringEncoder(msg),
		Headers: []sarama.RecordHeader{{Key: []byte("SomeKey"), Value: []byte("SomeValue")}},
	}

	self.producer.Input() <- &producerMsg
	successMsg := <-self.producer.Successes()
	log.Println("Successful to write message, offset:", successMsg.Offset)

	//err := self.producer.Close()
	//if err != nil {
	//	log.Fatalln("Failed to close producer:", err)
	//}
	return nil
}
