package controllers

import (
	"context"
	"database/sql"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/db_models"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/user_states"
)

// mock for session storage could be mongo or redis in production
type SessionData struct {
	UserId           int64
	State            user_states.UserState
	Currency         string
	SelectedData     string
	SelectedCategory string
}

type UserDBSqlDB interface {
	QueryRow(query string, args ...any) *sql.Row
	Exec(query string, args ...any) (sql.Result, error)
	Query(query string, args ...any) (*sql.Rows, error)
}

type UserDBController struct {
	db UserDBSqlDB
}

func NewUserDB(db UserDBSqlDB) *UserDBController {
	return &UserDBController{db: db}
}

func (self *UserDBController) selectUser(ctx context.Context, userID int64) (*db_models.User, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "selectUser")

	defer span.Finish()
	const selectQuery = `
		select id,     
			state,
			currency, 
		    selected_category
		from public.users
		where id = $1;
	`
	var userData db_models.User
	err := self.db.QueryRow(selectQuery, userID).Scan(&userData.Id, &userData.State, &userData.Currency, &userData.SelectedCategory)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &userData, err
}

func (self *UserDBController) insertUser(ctx context.Context, userData db_models.User) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "insertUser")
	defer span.Finish()
	const insertQuery = `
		insert into public.users(
			id, 
			state,
			currency, 
		    selected_category
		) values (
			$1, $2, $3, $4 
		);
    `
	_, err := self.db.Exec(insertQuery,
		userData.Id,
		userData.State,
		userData.Currency,
		userData.SelectedCategory,
	)
	return err
}

func (self *UserDBController) updateUser(ctx context.Context, userData db_models.User) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "updateUser")
	defer span.Finish()
	const updateQuery = `
		UPDATE public.users SET state = $2, currency = $3, selected_category = $4
  		WHERE id = $1;
    `
	_, err := self.db.Exec(updateQuery,
		userData.Id,
		userData.State,
		userData.Currency,
		userData.SelectedCategory,
	)
	return err
}

func (self *UserDBController) PutUserData(ctx context.Context, userID int64, data SessionData) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PutUserData")
	defer span.Finish()
	userData, err := self.selectUser(ctx, userID)
	if err != nil {
		return err
	}
	DbUser := db_models.User{
		Id:               userID,
		State:            int64(data.State),
		Currency:         data.Currency,
		SelectedCategory: data.SelectedCategory,
	}
	if userData == nil {
		err = self.insertUser(ctx, DbUser)
		if err != nil {
			return err
		}
	} else {
		err = self.updateUser(ctx, DbUser)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self *UserDBController) GetUserData(ctx context.Context, userID int64) (SessionData, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetUserData")
	defer span.Finish()
	userData, err := self.selectUser(ctx, userID)
	if err != nil {
		return SessionData{}, nil
	}

	if userData == nil {
		err = self.insertUser(ctx, db_models.User{
			Id:               userID,
			State:            0,
			Currency:         "RUB",
			SelectedCategory: "",
		})
		if err != nil {
			return SessionData{}, err
		}
	}
	userData, err = self.selectUser(ctx, userID)
	if err != nil {
		return SessionData{}, nil
	}
	return SessionData{
		UserId:           userID,
		State:            user_states.UserState(userData.State),
		Currency:         userData.Currency,
		SelectedCategory: userData.SelectedCategory,
	}, nil
}

func (self *UserDBController) ChangeState(ctx context.Context, userID int64, state user_states.UserState) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "ChangeState")
	defer span.Finish()
	sessionData, err := self.GetUserData(ctx, userID)
	if err != nil {
		return err
	}
	sessionData.State = state
	err = self.PutUserData(ctx, userID, sessionData)
	return err
}

func (self *UserDBController) SetCurrency(ctx context.Context, userID int64, currency string) error {
	span, ctx := opentracing.StartSpanFromContext(ctx, "SetCurrency")
	defer span.Finish()
	sessionData, err := self.GetUserData(ctx, userID)
	if err != nil {
		return err
	}
	sessionData.Currency = currency
	err = self.PutUserData(ctx, userID, sessionData)
	return err
}

func (self *UserDBController) GetCurrency(ctx context.Context, userID int64) (string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetCurrency")
	defer span.Finish()
	sessionData, err := self.GetUserData(ctx, userID)
	if err != nil {
		return "", err
	}
	if sessionData.Currency == "" {
		return "RUB", nil
	}
	return sessionData.Currency, nil
}
