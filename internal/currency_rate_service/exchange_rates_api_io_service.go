package currency_rate_service

import (
	"context"

	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/apilayerintegration"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"go.uber.org/zap"
)

type ExchangeRateFetcherGateway interface {
	GetCurrencyRates() (apilayerintegration.Response, error)
}

type ExchangeRateFetcherStorage interface {
	AddCurrencyRate(ctx context.Context, CNY, EUR, USD float64, date string) error
}

type ExchangeRateUpdateSvc struct {
	gateway ExchangeRateFetcherGateway
	storage ExchangeRateFetcherStorage
}

func New(gateway ExchangeRateFetcherGateway, storage ExchangeRateFetcherStorage) *ExchangeRateUpdateSvc {
	return &ExchangeRateUpdateSvc{gateway: gateway, storage: storage}
}

func (svc *ExchangeRateUpdateSvc) UpdateCurrencyRates() {
	logger.Logger.Info("UpdateCurrencyRates ... ")
	rates, err := svc.gateway.GetCurrencyRates()
	if err != nil {
		logger.Logger.Warn("GetCurrencyRates error", zap.Error(err))
		return
	}
	err = svc.storage.AddCurrencyRate(context.Background(), rates.Rates.CNY, rates.Rates.EUR, rates.Rates.USD, rates.Date)
	if err != nil {
		logger.Logger.Error("AddCurrencyRate error", zap.Error(err))
		return
	}
}
