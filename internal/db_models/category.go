package db_models

import "database/sql"

type Category struct {
	Id       int64
	UserId   int64
	Name     string
	LimitRub sql.NullInt64
}
