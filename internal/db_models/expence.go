package db_models

import "time"

type Expense struct {
	ID         int64
	CategoryId int64
	Amount     int64
	Date       time.Time
}
