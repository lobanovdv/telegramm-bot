package db_models

import (
	"time"
)

type Rate struct {
	ID        int64
	Code      string
	Rate      float64
	Date      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}
