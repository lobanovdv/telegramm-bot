package db_models

type User struct {
	Id               int64
	State            int64
	Currency         string
	SelectedCategory string
}
