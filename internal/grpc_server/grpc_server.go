package grpc_server

import (
	"context"
	"strings"

	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/api/send_message"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/messages"
)

type MessageSender interface {
	SendMessage(ctx context.Context, text string, userID int64) error
	ShowCurrencyKeyboard(ctx context.Context, userID int64) error
	ShowReportPeriodKeyboard(ctx context.Context, userID int64) error
	ShowCategoriesKeyboard(ctx context.Context, userID int64, categories []string) error
}

// server is used to implement helloworld.GreeterServer.
type Server struct {
	send_message.UnimplementedSendBotCommandServer
	TgClient MessageSender
}

// SayHello implements helloworld.GreeterServer
func (s *Server) SendBotCommand(ctx context.Context, in *send_message.SendBotCommandRequest) (*send_message.SendBotCommandReply, error) {
	userId := in.GetUserId()
	keyboard := messages.KeyboardType(in.GetKeyboardType())
	msg := in.GetMessage()

	result := &send_message.SendBotCommandReply{Status: "OK"}
	tgr := s.TgClient
	switch keyboard {
	case messages.NoKeyboard:
		return result, tgr.SendMessage(ctx, msg, userId)
	case messages.CurrencyKeyboard:
		return result, tgr.ShowCurrencyKeyboard(ctx, userId)
	case messages.PeriodKeyboard:
		return result, tgr.ShowReportPeriodKeyboard(ctx, userId)
	case messages.CategoryKeyboard:
		categories := strings.Split(msg, "✾")
		return result, tgr.ShowCategoriesKeyboard(ctx, userId, categories)
	}
	logger.Logger.Info("GRPC Received: " + in.GetMessage())
	return &send_message.SendBotCommandReply{Status: "OK"}, nil
}
