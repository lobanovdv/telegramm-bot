package logger

import (
	"log"

	"github.com/uber/jaeger-client-go/config"
	"go.uber.org/zap"
)

var Logger *zap.Logger

const (
	subsystemKey                     = "subsystem"
	subsystemTypeCurrencyIntegration = "currency_integration"
	subsystemTypeMain                = "main"
	subsystemTypeTgintegration       = "tg"
)

var CurrencyIntegration = zap.String(subsystemKey, subsystemTypeCurrencyIntegration)
var TgIntegration = zap.String(subsystemKey, subsystemTypeTgintegration)

func init() {
	localLogger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatal("logger init", err)
	}

	Logger = localLogger

	cfg := config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
	}
	_, err = cfg.InitGlobalTracer("telegram-bot")
	if err != nil {
		Logger.Fatal("Cannot init tracing", zap.Error(err))
	}
}
