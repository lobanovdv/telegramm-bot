package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var SummaryResponseTime = promauto.NewSummary(prometheus.SummaryOpts{
	Namespace: "ozon",
	Subsystem: "http",
	Name:      "summary_response_time_seconds",
	Objectives: map[float64]float64{
		0.5:  0.1,
		0.9:  0.01,
		0.99: 0.001,
	},
})
var HistogramResponseTime = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Namespace: "ozon",
		Subsystem: "http",
		Name:      "histogram_response_time_seconds",
		Buckets:   []float64{0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 2},
		// Buckets: prometheus.ExponentialBucketsRange(0.0001, 2, 16),
	},
	[]string{"code"},
)

type CommandType string

const (
	MetricsCommandType CommandType = "commandType"
)
