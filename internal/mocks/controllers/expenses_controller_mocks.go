// Code generated by MockGen. DO NOT EDIT.
// Source: internal/controllers/expenses_controller.go

// Package mock_controllers is a generated GoMock package.
package mock_controllers

import (
	context "context"
	reflect "reflect"
	time "time"

	gomock "github.com/golang/mock/gomock"
	expenses "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/expenses"
)

// MockDBStorage is a mock of DBStorage interface.
type MockDBStorage struct {
	ctrl     *gomock.Controller
	recorder *MockDBStorageMockRecorder
}

// MockDBStorageMockRecorder is the mock recorder for MockDBStorage.
type MockDBStorageMockRecorder struct {
	mock *MockDBStorage
}

// NewMockDBStorage creates a new mock instance.
func NewMockDBStorage(ctrl *gomock.Controller) *MockDBStorage {
	mock := &MockDBStorage{ctrl: ctrl}
	mock.recorder = &MockDBStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockDBStorage) EXPECT() *MockDBStorageMockRecorder {
	return m.recorder
}

// AddCurrencyRate mocks base method.
func (m *MockDBStorage) AddCurrencyRate(ctx context.Context, CNY, EUR, USD float64, date string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddCurrencyRate", ctx, CNY, EUR, USD, date)
	ret0, _ := ret[0].(error)
	return ret0
}

// AddCurrencyRate indicates an expected call of AddCurrencyRate.
func (mr *MockDBStorageMockRecorder) AddCurrencyRate(ctx, CNY, EUR, USD, date interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddCurrencyRate", reflect.TypeOf((*MockDBStorage)(nil).AddCurrencyRate), ctx, CNY, EUR, USD, date)
}

// AddExpense mocks base method.
func (m *MockDBStorage) AddExpense(ctx context.Context, expense expenses.Expense) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddExpense", ctx, expense)
	ret0, _ := ret[0].(error)
	return ret0
}

// AddExpense indicates an expected call of AddExpense.
func (mr *MockDBStorageMockRecorder) AddExpense(ctx, expense interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddExpense", reflect.TypeOf((*MockDBStorage)(nil).AddExpense), ctx, expense)
}

// GetCategoriesList mocks base method.
func (m *MockDBStorage) GetCategoriesList(ctx context.Context, userID int64) ([]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCategoriesList", ctx, userID)
	ret0, _ := ret[0].([]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCategoriesList indicates an expected call of GetCategoriesList.
func (mr *MockDBStorageMockRecorder) GetCategoriesList(ctx, userID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCategoriesList", reflect.TypeOf((*MockDBStorage)(nil).GetCategoriesList), ctx, userID)
}

// GetCategoryLimit mocks base method.
func (m *MockDBStorage) GetCategoryLimit(ctx context.Context, categoryName string, userID int64) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCategoryLimit", ctx, categoryName, userID)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCategoryLimit indicates an expected call of GetCategoryLimit.
func (mr *MockDBStorageMockRecorder) GetCategoryLimit(ctx, categoryName, userID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCategoryLimit", reflect.TypeOf((*MockDBStorage)(nil).GetCategoryLimit), ctx, categoryName, userID)
}

// GetExpenses mocks base method.
func (m *MockDBStorage) GetExpenses(ctx context.Context, userID int64, dateGt, dateLt time.Time) ([]expenses.Expense, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetExpenses", ctx, userID, dateGt, dateLt)
	ret0, _ := ret[0].([]expenses.Expense)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetExpenses indicates an expected call of GetExpenses.
func (mr *MockDBStorageMockRecorder) GetExpenses(ctx, userID, dateGt, dateLt interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetExpenses", reflect.TypeOf((*MockDBStorage)(nil).GetExpenses), ctx, userID, dateGt, dateLt)
}

// GetLastCurrencyRate mocks base method.
func (m *MockDBStorage) GetLastCurrencyRate(ctx context.Context, currency string) (float64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetLastCurrencyRate", ctx, currency)
	ret0, _ := ret[0].(float64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetLastCurrencyRate indicates an expected call of GetLastCurrencyRate.
func (mr *MockDBStorageMockRecorder) GetLastCurrencyRate(ctx, currency interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetLastCurrencyRate", reflect.TypeOf((*MockDBStorage)(nil).GetLastCurrencyRate), ctx, currency)
}

// SetLimit mocks base method.
func (m *MockDBStorage) SetLimit(ctx context.Context, amount int64, category string, userID int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetLimit", ctx, amount, category, userID)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetLimit indicates an expected call of SetLimit.
func (mr *MockDBStorageMockRecorder) SetLimit(ctx, amount, category, userID interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetLimit", reflect.TypeOf((*MockDBStorage)(nil).SetLimit), ctx, amount, category, userID)
}

// MockFastCache is a mock of FastCache interface.
type MockFastCache struct {
	ctrl     *gomock.Controller
	recorder *MockFastCacheMockRecorder
}

// MockFastCacheMockRecorder is the mock recorder for MockFastCache.
type MockFastCacheMockRecorder struct {
	mock *MockFastCache
}

// NewMockFastCache creates a new mock instance.
func NewMockFastCache(ctrl *gomock.Controller) *MockFastCache {
	mock := &MockFastCache{ctrl: ctrl}
	mock.recorder = &MockFastCacheMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockFastCache) EXPECT() *MockFastCacheMockRecorder {
	return m.recorder
}

// Del mocks base method.
func (m *MockFastCache) Del(ctx context.Context, keys ...string) error {
	m.ctrl.T.Helper()
	varargs := []interface{}{ctx}
	for _, a := range keys {
		varargs = append(varargs, a)
	}
	ret := m.ctrl.Call(m, "Del", varargs...)
	ret0, _ := ret[0].(error)
	return ret0
}

// Del indicates an expected call of Del.
func (mr *MockFastCacheMockRecorder) Del(ctx interface{}, keys ...interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	varargs := append([]interface{}{ctx}, keys...)
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Del", reflect.TypeOf((*MockFastCache)(nil).Del), varargs...)
}

// GetValue mocks base method.
func (m *MockFastCache) GetValue(ctx context.Context, key string) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetValue", ctx, key)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetValue indicates an expected call of GetValue.
func (mr *MockFastCacheMockRecorder) GetValue(ctx, key interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetValue", reflect.TypeOf((*MockFastCache)(nil).GetValue), ctx, key)
}

// SetValue mocks base method.
func (m *MockFastCache) SetValue(ctx context.Context, key, value string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SetValue", ctx, key, value)
	ret0, _ := ret[0].(error)
	return ret0
}

// SetValue indicates an expected call of SetValue.
func (mr *MockFastCacheMockRecorder) SetValue(ctx, key, value interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetValue", reflect.TypeOf((*MockFastCache)(nil).SetValue), ctx, key, value)
}
