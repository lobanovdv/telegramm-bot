package expenses

import "time"

type Expense struct {
	ID       int64
	Category string
	UserID   int64
	Amount   int64
	Date     time.Time
}

var idAutoIncrement int64 = 0

func New(date time.Time, category string, amount int64, userID int64) *Expense {
	idAutoIncrement += 1
	return &Expense{
		ID:       idAutoIncrement,
		Category: category,
		UserID:   userID,
		Amount:   amount,
		Date:     date,
	}
}
