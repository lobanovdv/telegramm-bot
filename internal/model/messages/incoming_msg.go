package messages

import (
	"context"
	"strconv"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/common"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/controllers"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/metrics"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/user_states"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/texts"
	"go.uber.org/zap"
)

type MessageSender interface {
	SendMessage(ctx context.Context, text string, userID int64) error
	ShowCurrencyKeyboard(ctx context.Context, userID int64) error
	ShowReportPeriodKeyboard(ctx context.Context, userID int64) error
	ShowCategoriesKeyboard(ctx context.Context, userID int64, categories []string) error
}

type ExpensesControllerI interface {
	GetLastCurrencyRate(ctx context.Context, currency string) (float64, error)
	GetReport(ctx context.Context, data string, userID int64, currencyRate float64) (string, error)
	AddExpense(ctx context.Context, amount int64, category string, userID int64, rate float64) (string, error)
	SetLimit(ctx context.Context, amount int64, category string, userID int64) error
	GetCategoriesList(ctx context.Context, userID int64) ([]string, error)
	AddExpenseWithDate(ctx context.Context, data string, userID int64, rate float64) error
}

type UserDBControllerI interface {
	GetUserData(ctx context.Context, userID int64) (controllers.SessionData, error)
	SetCurrency(ctx context.Context, userID int64, currency string) error
	ChangeState(ctx context.Context, userID int64, state user_states.UserState) error
	PutUserData(ctx context.Context, userID int64, data controllers.SessionData) error
	GetCurrency(ctx context.Context, userID int64) (string, error)
}

type Model struct {
	tgClient           MessageSender
	expensesController ExpensesControllerI
	userController     UserDBControllerI
}

func New(tgClient MessageSender, expensesController ExpensesControllerI, userController UserDBControllerI) *Model {
	return &Model{
		tgClient:           tgClient,
		expensesController: expensesController,
		userController:     userController,
	}
}

type Message struct {
	Text   string
	UserID int64
}

const (
	StartCommandPrefix       = `/start`
	HelpCommandPrefix        = `/help`
	AddCommandPrefix         = `/add`
	AddWithDateCommandPrefix = `/add_with_date`
	ReportCommandPrefix      = `/report`
	ChangeCurrencyCommand    = `/change_currency`
	SetLimitCommand          = `/set_limit`
)

type KeyboardType int

const (
	NoKeyboard KeyboardType = iota
	CurrencyKeyboard
	PeriodKeyboard
	CategoryKeyboard
)

func (s *Model) changeStateForDefer(ctx context.Context, userID int64) {
	err := s.userController.ChangeState(ctx, userID, user_states.Root)
	if err != nil {
		logger.Logger.Error("defer ChangeState", zap.Error(err))
	}
}

func (s *Model) IncomingMessageProcessing(ctx context.Context, msg Message) (context.Context, KeyboardType, string, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "incomingMessageProcessing")
	defer span.Finish()
	ctx = context.WithValue(ctx, metrics.MetricsCommandType, "None")
	sessionData, err := s.userController.GetUserData(ctx, msg.UserID)
	if err != nil {
		return ctx, NoKeyboard, "", err
	}
	rate, err := s.expensesController.GetLastCurrencyRate(ctx, sessionData.Currency)
	if err != nil {
		return ctx, NoKeyboard, "", err
	}
	if sessionData.State == user_states.WaitForCurrency {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForCurrency")
		if msg.Text == "USD" || msg.Text == "CNY" || msg.Text == "EUR" || msg.Text == "RUB" {
			err := s.userController.SetCurrency(ctx, msg.UserID, msg.Text)
			if err != nil {
				return ctx, NoKeyboard, "", err
			}
		}
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.Root)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		currency, err := s.userController.GetCurrency(ctx, msg.UserID)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		return ctx, NoKeyboard, texts.CurrentCurrencyText + currency, nil
	}

	if sessionData.State == user_states.WaitForReportPeriod {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForReportPeriod")
		defer s.changeStateForDefer(ctx, msg.UserID)

		if msg.Text == "week" || msg.Text == "month" || msg.Text == "year" {
			result, err := s.expensesController.GetReport(ctx, msg.Text, msg.UserID, rate)
			if err != nil {
				return ctx, 0, "", err
			}
			return ctx, NoKeyboard, texts.ExpensesForPeriodText + result, nil
		}
		return ctx, NoKeyboard, texts.IncorrectPeriodErrorText, nil
	}

	if sessionData.State == user_states.WaitForCategory {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForCategory")
		sessionData.SelectedCategory = msg.Text
		sessionData.SelectedData = time.Now().Format(common.CommonDateFormat)
		err = s.userController.PutUserData(ctx, msg.UserID, sessionData)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForAmount)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		return ctx, NoKeyboard, texts.RequestCategoryText, nil
	}

	if sessionData.State == user_states.WaitForAmount {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForAmount")
		amount, err := strconv.ParseInt(msg.Text, 10, 0)
		defer s.changeStateForDefer(ctx, msg.UserID)

		if err != nil {
			return ctx, 0, "", err
		}
		remaining, err := s.expensesController.AddExpense(ctx, amount, sessionData.SelectedCategory, msg.UserID, rate)
		if err != nil {
			return ctx, 0, "", err
		}
		if remaining == "" {
			return ctx, NoKeyboard, texts.OkMessageText, nil
		} else {
			return ctx, NoKeyboard, remaining, nil
		}
	}

	if sessionData.State == user_states.WaitForSetLimitCategory {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForSetLimitCategory")
		sessionData.SelectedCategory = msg.Text
		sessionData.SelectedData = time.Now().Format(common.CommonDateFormat)
		err = s.userController.PutUserData(ctx, msg.UserID, sessionData)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForSetLimitAmount)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		return ctx, NoKeyboard, texts.RequestLimitAmount, nil
	}

	if sessionData.State == user_states.WaitForSetLimitAmount {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "WaitForSetLimitAmount")
		amount, err := strconv.ParseInt(msg.Text, 10, 0)
		defer s.changeStateForDefer(ctx, msg.UserID)
		if err != nil {
			return ctx, 0, "", err
		}
		err = s.expensesController.SetLimit(ctx, amount, sessionData.SelectedCategory, msg.UserID)
		if err != nil {
			return ctx, 0, "", err
		}
		return ctx, NoKeyboard, texts.OkMessageText, nil
	}

	if msg.Text == SetLimitCommand {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "SetLimitCommand")
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForSetLimitCategory)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		categories, err := s.expensesController.GetCategoriesList(ctx, msg.UserID)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		justStringCategories := strings.Join(categories, "✾")
		return ctx, CategoryKeyboard, justStringCategories, nil
	}

	if msg.Text == StartCommandPrefix || msg.Text == HelpCommandPrefix {
		return ctx, NoKeyboard, texts.HelpMessageText, nil
	}

	if strings.HasPrefix(msg.Text, AddWithDateCommandPrefix) {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "AddWithDateCommandPrefix")
		err := s.expensesController.AddExpenseWithDate(ctx, msg.Text[len(AddWithDateCommandPrefix):], msg.UserID, rate)
		if err != nil {
			return ctx, 0, "", err
		}
		return ctx, NoKeyboard, texts.OkMessageText, nil
	}

	if strings.HasPrefix(msg.Text, ChangeCurrencyCommand) {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "ChangeCurrencyCommand")
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForCurrency)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		return ctx, CurrencyKeyboard, "", nil
	}

	if strings.HasPrefix(msg.Text, AddCommandPrefix) {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "AddCommandPrefix")
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForCategory)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		categories, err := s.expensesController.GetCategoriesList(ctx, msg.UserID)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		justStringCategories := strings.Join(categories, "✾")
		return ctx, CategoryKeyboard, justStringCategories, nil
	}

	if strings.HasPrefix(msg.Text, ReportCommandPrefix) {
		ctx = context.WithValue(ctx, metrics.MetricsCommandType, "ReportCommandPrefix")
		err = s.userController.ChangeState(ctx, msg.UserID, user_states.WaitForReportPeriod)
		if err != nil {
			return ctx, NoKeyboard, "", err
		}
		return ctx, PeriodKeyboard, "", nil
	}
	return ctx, NoKeyboard, texts.SomethingWentWrongText, nil
}

func (s *Model) IncomingMessage(ctx context.Context, msg Message) (context.Context, error) {
	logger.Logger.Info("s.IncomingMessage", zap.Any("message", msg))
	span, ctx := opentracing.StartSpanFromContext(
		ctx,
		"request "+strconv.FormatInt(msg.UserID, 10)+" "+msg.Text,
	)
	defer span.Finish()
	spanContext, _ := span.Context().(jaeger.SpanContext)
	ctx, keyboard, message, err := s.IncomingMessageProcessing(ctx, msg)
	logger.Logger.Info(
		"s.incomingMessageProcessing result",
		zap.Any("keyboard", keyboard),
		zap.String("message", message),
		zap.String("trace_id", spanContext.TraceID().String()),
		zap.Error(err),
	)

	if err != nil {
		logger.Logger.Error("error during incomingMessageProcessing", zap.Error(err))
		return ctx, s.tgClient.SendMessage(ctx, texts.SomethingWentWrongText, msg.UserID)
	}
	switch keyboard {
	case NoKeyboard:
		return ctx, s.tgClient.SendMessage(ctx, message, msg.UserID)
	case CurrencyKeyboard:
		return ctx, s.tgClient.ShowCurrencyKeyboard(ctx, msg.UserID)
	case PeriodKeyboard:
		return ctx, s.tgClient.ShowReportPeriodKeyboard(ctx, msg.UserID)
	case CategoryKeyboard:
		categories := strings.Split(message, "✾")
		return ctx, s.tgClient.ShowCategoriesKeyboard(ctx, msg.UserID, categories)
	}
	return ctx, err

}
