package messages

import (
	"context"
	"strconv"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/controllers"
	_ "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	mocks_messages "gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/mocks/messages"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/model/user_states"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/texts"
)

func addItem(t *testing.T,
	userController *mocks_messages.MockUserDBControllerI,
	expensesController *mocks_messages.MockExpensesControllerI,
	sender *mocks_messages.MockMessageSender,
	model *Model,
	userId int64, cetegory string, amount int64) {

	ctx := context.Background()
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123)}, nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.UserState(3))
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().GetCategoriesList(gomock.Any(), int64(123)).Return([]string{}, nil)
	sender.EXPECT().ShowCategoriesKeyboard(gomock.Any(), userId, gomock.Any())
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/add",
		UserID: userId,
	})
	assert.NoError(t, err)

	userController.EXPECT().PutUserData(gomock.Any(), gomock.Any(), gomock.Any())
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.WaitForCategory}, nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.WaitForAmount)
	sender.EXPECT().SendMessage(gomock.Any(), "print category amount", userId)
	_, err = model.IncomingMessage(ctx, Message{
		Text:   cetegory,
		UserID: userId,
	})
	assert.NoError(t, err)

	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().AddExpense(gomock.Any(), amount, cetegory, userId, float64(1))
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{
		UserId: int64(123), State: user_states.WaitForAmount, SelectedCategory: cetegory}, nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.Root)
	sender.EXPECT().SendMessage(gomock.Any(), "Done!", userId)
	_, err = model.IncomingMessage(ctx, Message{
		Text:   strconv.FormatInt(amount, 10),
		UserID: 123,
	})
	assert.NoError(t, err)
}

func TestOnStartCommandShouldAnswerWithIntroMessage(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123)}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	sender.EXPECT().SendMessage(gomock.Any(), texts.HelpMessageText, int64(123))

	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/start",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func TestOnAddCommand(t *testing.T) {
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	addItem(t, userController, expensesController, sender, model, int64(123), "grosseries", int64(100))
}

func TestOnAddWithDateCommand(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123)}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().AddExpenseWithDate(gomock.Any(), gomock.Any(), int64(123), float64(1))
	sender.EXPECT().SendMessage(gomock.Any(), texts.OkMessageText, int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/add_with_date 20.02.2002 100 grosseries",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func TestOnReportOnAddWithDateCommand(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123)}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().AddExpenseWithDate(gomock.Any(), gomock.Any(), int64(123), float64(1))
	sender.EXPECT().SendMessage(gomock.Any(), texts.OkMessageText, int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/add_with_date 20.02.2002 100 grosseries",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func TestOnAddCommandIncorrectParams(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123)}, nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.UserState(3))
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().GetCategoriesList(gomock.Any(), int64(123)).Return([]string{}, nil)
	sender.EXPECT().ShowCategoriesKeyboard(gomock.Any(), int64(123), gomock.Any())
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/add",
		UserID: int64(123),
	})
	assert.NoError(t, err)

	userController.EXPECT().PutUserData(gomock.Any(), gomock.Any(), gomock.Any())
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.WaitForCategory}, nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.WaitForAmount)
	sender.EXPECT().SendMessage(gomock.Any(), "print category amount", int64(123))
	_, err = model.IncomingMessage(ctx, Message{
		Text:   "car",
		UserID: int64(123),
	})
	assert.NoError(t, err)

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.WaitForAmount}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.Root)
	sender.EXPECT().SendMessage(gomock.Any(), texts.SomethingWentWrongText, int64(123))
	_, err = model.IncomingMessage(ctx, Message{
		Text:   "asdasd10011",
		UserID: 123,
	})
	assert.NoError(t, err)
}

func doReportCall(t *testing.T,
	userController *mocks_messages.MockUserDBControllerI,
	expensesController *mocks_messages.MockExpensesControllerI,
	sender *mocks_messages.MockMessageSender,
	model *Model,
	userId int64) {

	ctx := context.Background()
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.Root}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.WaitForReportPeriod)
	sender.EXPECT().ShowReportPeriodKeyboard(gomock.Any(), int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/report",
		UserID: userId,
	})
	assert.NoError(t, err)
}

func TestReportCommand(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	addItem(t, userController, expensesController, sender, model, int64(123), "grosseries", int64(100))

	doReportCall(t, userController, expensesController, sender, model, int64(123))

	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.WaitForReportPeriod}, nil)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	expensesController.EXPECT().GetReport(gomock.Any(), "week", int64(123), float64(1)).Return("grosseries: 100.00\n", nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.Root)
	sender.EXPECT().SendMessage(gomock.Any(), texts.ExpensesForPeriodText+"grosseries: 100.00\n", int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "week",
		UserID: 123,
	})
	assert.NoError(t, err)
}

func TestOnUnknownCommandShouldAnswerWithHelpMessage(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.Root}, nil)
	sender.EXPECT().SendMessage(gomock.Any(), texts.SomethingWentWrongText, int64(123))

	_, err := model.IncomingMessage(ctx, Message{
		Text:   "some text",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func TestIncorrectCommandFormat(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	doReportCall(t, userController, expensesController, sender, model, int64(123))

	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.Root}, nil)
	sender.EXPECT().SendMessage(gomock.Any(), gomock.Any(), int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "asdasd",
		UserID: 123,
	})

	assert.NoError(t, err)
}

func TestReportWithUSDCurrency(t *testing.T) {
	ctx := context.Background()
	ctrl := gomock.NewController(t)
	expensesController := mocks_messages.NewMockExpensesControllerI(ctrl)
	userController := mocks_messages.NewMockUserDBControllerI(ctrl)
	sender := mocks_messages.NewMockMessageSender(ctrl)

	model := New(sender, expensesController, userController)

	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.WaitForCurrency)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.Root}, nil)
	sender.EXPECT().ShowCurrencyKeyboard(gomock.Any(), int64(123))
	_, err := model.IncomingMessage(ctx, Message{
		Text:   "/change_currency",
		UserID: 123,
	})
	assert.NoError(t, err)

	userController.EXPECT().SetCurrency(gomock.Any(), int64(123), "USD")
	userController.EXPECT().GetCurrency(gomock.Any(), gomock.Any()).Return("USD", nil)
	userController.EXPECT().ChangeState(gomock.Any(), int64(123), user_states.Root)
	expensesController.EXPECT().GetLastCurrencyRate(gomock.Any(), "").Return(float64(1), nil)
	userController.EXPECT().GetUserData(gomock.Any(), int64(123)).Return(controllers.SessionData{UserId: int64(123), State: user_states.WaitForCurrency}, nil)
	sender.EXPECT().SendMessage(gomock.Any(), texts.CurrentCurrencyText+"USD", int64(123))
	_, err = model.IncomingMessage(ctx, Message{
		Text:   "USD",
		UserID: 123,
	})
	assert.NoError(t, err)
}
