package user_states

type UserState int

const (
	Root UserState = iota
	WaitForCurrency
	WaitForReportPeriod
	WaitForCategory
	WaitForAmount
	WaitForSetLimitCategory
	WaitForSetLimitAmount
)
