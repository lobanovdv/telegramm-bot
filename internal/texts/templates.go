package texts

import (
	"bytes"
	"text/template"
)

// Todo config file maybe with localization
const (
	HelpMessageText = `Bot supports next commands: \n
/help - return help message \n 
/add amount category - add expense to inputed category \n 
/add_with_date dd.mm.yyyy amount category - add expense to inputed category with needed date \n
/report week|month|year - return categories report \n
/change_currency - opens change currency dialog`
	OkMessageText            = `Done!`
	SomethingWentWrongText   = `something went wrong, please see /help command`
	ExpensesForPeriodText    = "expenses for the period:\n"
	IncorrectPeriodErrorText = "incorrect period value"
	ChooseCategoryText       = "choose category or type new one"
	CurrentCurrencyText      = "current currency is "
	RequestCategoryText      = "print category amount"
	RequestLimitAmount       = "print limit amount"
)

type reportRowTemplateData struct {
	Category string
	Amount   string
}

var reportRowMessage, _ = template.New("").Parse("{{.Category}}: {{.Amount}}\n")

func CreateReportRowMessage(category, amount string) (string, error) {
	var tpl bytes.Buffer
	if err := reportRowMessage.Execute(&tpl, reportRowTemplateData{
		Category: category,
		Amount:   amount,
	}); err != nil {
		return "", err
	}
	return tpl.String(), nil
}
