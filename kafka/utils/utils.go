package utils

import (
	"fmt"

	"github.com/Shopify/sarama"
	"gitlab.ozon.dev/lobanovdmitry777/telegram-bot/internal/logger"
	"go.uber.org/zap"
)

var (
	KafkaTopic  = "commands"
	BrokersList = []string{"localhost:9092"}
)

func NewProducer(brokerList []string) (sarama.AsyncProducer, error) {
	config := sarama.NewConfig()
	config.Version = sarama.V2_5_0_0
	// So we can know the partition and offset of messages.
	config.Producer.Return.Successes = true

	producer, err := sarama.NewAsyncProducer(brokerList, config)
	if err != nil {
		return nil, fmt.Errorf("starting Sarama producer: %w", err)
	}

	go func() {
		for err := range producer.Errors() {
			logger.Logger.Error("Failed to write message:", zap.Error(err))
		}
	}()

	return producer, nil
}
