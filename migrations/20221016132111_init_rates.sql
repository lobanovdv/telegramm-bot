-- +goose Up
-- +goose StatementBegin
create table public.rates
(
    id integer generated always as identity
        primary key,
    code       text      not null,
    rate       float     not null,
    date       text      not null,
    created_at timestamp not null,
    updated_at timestamp,
    deleted_at timestamp
);
create index rates__code__index
    on rates using hash(code);  -- поиск по последниму и коду
create index rates__date__index
    on rates (date desc);       -- поиск последнего + проверка что есть записи за день
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table public.rates;
-- +goose StatementEnd
