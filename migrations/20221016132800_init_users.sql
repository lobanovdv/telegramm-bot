-- +goose Up
-- +goose StatementBegin
create table public.users
(
    id integer primary key not null,
    state             integer,
    currency          text,
    selected_category text
);
-- поиск только по ид там индекс по дефолту

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table public.users;
-- +goose StatementEnd
