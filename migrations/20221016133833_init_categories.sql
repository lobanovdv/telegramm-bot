-- +goose Up
-- +goose StatementBegin
create table public.categories (
                                   id integer generated always as identity
                                       primary key,
                                   user_id integer,
                                   name text,
                                   limit_rub integer,
                                   foreign key (user_id) references public.users (id)
                                       match simple on update no action on delete no action
);
-- поиск только по PK FK там индекс по дефолту

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table public.categories
-- +goose StatementEnd
