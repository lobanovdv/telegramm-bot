-- +goose Up
-- +goose StatementBegin
create table public.expenses
(
    id integer generated always as identity
        primary key,
    category_id   integer,
    amount_rub     integer,
    date       DATE,
    foreign key (category_id) references public.categories (id)
        match simple on update no action on delete no action
);
create index expenses__date__index
    on expenses (date desc); -- фильтры по периодам.
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
drop table public.expenses;
-- +goose StatementEnd
